package cn.zglong.rcap.config;

import cn.hutool.extra.mail.MailUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

/**
 * @author zglong
 * @version 1.0
 * @date 2020/5/7/007 10:04
 */
@Component
@Slf4j
public class SendingIPAddress implements CommandLineRunner {
    @Override
    public void run(String... args) {
        log.info("邮件发送中");
        try {
            Thread.sleep(10000);
            MailUtil.send("2551332579@qq.com",  "RCAP", InetAddress.getLocalHost().toString(),false);
            log.info("success");
        } catch (Exception e) {
            e.printStackTrace();
            MailUtil.send("2551332579@qq.com","RCAP",  e.getMessage(), false);
            log.info("error");
        }
    }
}
