package cn.zglong.rcap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;

@SpringBootApplication
@RestController
@RequestMapping
public class RCAP {

    public static void main(String[] args) {
        SpringApplication.run(RCAP.class, args);
    }

    @GetMapping("/index")
    public String init() throws UnknownHostException {
        return "index";
    }
}
