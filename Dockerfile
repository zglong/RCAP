FROM openjdk:8-jdk-alpine
ADD target/*.jar /home/app.jar
ENTRYPOINT ["java","-jar","/home/app.jar"]